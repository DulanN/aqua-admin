<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=array(
            array(
                'name'=>'Admin',
                'email'=>'admin@admin.com',
                'password'=>Hash::make('password'),
                'role'=>'admin',
            ),
            array(
                'name'=>'User',
                'email'=>'user@user.com',
                'password'=>Hash::make('password'),
                'role'=>'user',
            ),
        );

        DB::table('users')->insert($data);
    }
}
