<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();

Route::get('/', function () {
    return redirect('login');
});

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' =>['auth','admin']],function() {


    Route::get('/categories', 'CategoryController@index');




    Route::get('/admin', 'AdminController@index')->name('admin');


    Route::get('/file-manager', function () {
        return view('backend.layouts.file-manager');
    })->name('file-manager');
// user route
    Route::resource('users', 'UsersController');
// Banner
    Route::resource('banner', 'BannerController');
// Brand
    Route::resource('brand', 'BrandController');
// Profile
    Route::get('/profile', 'AdminController@profile')->name('admin-profile');
    Route::post('/profile/{id}', 'AdminController@profileUpdate')->name('profile-update');
// Category
    Route::resource('/category', 'CategoryController');

    Route::resource('/variety', 'varietyController');
// Product
    Route::resource('/product', 'ProductController');
// Ajax for sub category
    Route::post('/category/{id}/child', 'CategoryController@getChildByParent');
// POST category
    Route::resource('/post-category', 'PostCategoryController');
// Post tag
    Route::resource('/post-tag', 'PostTagController');
// Post
    Route::resource('/post', 'PostController');
// Message
    Route::resource('/message', 'MessageController');
    Route::get('/message/five', 'MessageController@messageFive')->name('messages.five');

// Order
    Route::resource('/order', 'OrderController');
// Shipping
    Route::resource('/shipping', 'ShippingController');
// Coupon
    Route::resource('/coupon', 'CouponController');
// Settings
    Route::get('settings', 'AdminController@settings')->name('settings');
    Route::post('setting/update', 'AdminController@settingsUpdate')->name('settings.update');

// Notification
    Route::get('/notification/{id}', 'NotificationController@show')->name('admin.notification');
    Route::get('/notifications', 'NotificationController@index')->name('all.notification');
    Route::delete('/notification/{id}', 'NotificationController@delete')->name('notification.delete');
// Password Change
    Route::get('change-password', 'AdminController@changePassword')->name('change.password.form');
    Route::post('change-password', 'AdminController@changPasswordStore')->name('change.password');



});
