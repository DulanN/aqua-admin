<?php

namespace App\Http\Controllers;
use App\Variety;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class varietyController extends Controller
{
    public function index()
    {
        $category=Variety::getAllCategory();

        // return $category;

        return view('backend.variety.index')->with('categories',$category);
    }

    public function create()
    {
        $parent_cats=Variety::orderBy('title','ASC')->get();
        $category=Category::where('is_parent',1)->get();
        return view('backend.variety.create')->with('parent_cats',$parent_cats)->with('categories',$category);
    }

    public function store(Request $request)
    {
        // return $request->all();
        $this->validate($request,[
            'title'=>'string|required',
            'summary'=>'string|nullable',
//            'photo'=>'required|max:2048',
            'status'=>'required|in:active,inactive',
           // 'is_parent'=>'sometimes|in:1',
            'parent_id'=>'required',
        ]);
        $data= $request->all();


        $slug=Str::slug($request->title);
        $count=Variety::where('slug',$slug)->count();
        if($count>0){
            $slug=$slug.'-'.date('ymdis').'-'.rand(0,999);
        }

//        $image_original = $request->file('photo');
//
//        $image = time().'.'.$request->photo->getClientOriginalName();
//        $data['photo'] = $image;
        $data['slug']= $slug;
//        $data['is_parent']=$request->input('is_parent',0);


        $status=Variety::create($data);

        if($status){
            request()->session()->flash('success','Variety successfully added');
        }

        else{
            request()->session()->flash('error','Error occurred, Please try again!');
        }

        return redirect()->route('variety.index');


    }
    public function destroy($id)
    {
        $category=Variety::findOrFail($id);
       // $child_cat_id=Variety::where('parent_id',$id)->pluck('id');
        // return $child_cat_id;
        $status=$category->delete();

        if($status){
//            if(count($child_cat_id)>0){
//                Variety::shiftChild($child_cat_id);
//            }
            request()->session()->flash('success','Variety successfully deleted');
        }
        else{
            request()->session()->flash('error','Error while deleting category');
        }
        return redirect()->route('variety.index');
    }

    public function edit($id)
    {
        $parent_cats=Variety::get();
        $category=Variety::findOrFail($id);

        $parent_category=Category::where('is_parent',1)->get();

        return view('backend.variety.edit')->with('category',$category)->with('parent_cats',$parent_cats)->with('categories',$parent_category);
    }
    public function update(Request $request, $id)
    {
        $category=Variety::findOrFail($id);
        $this->validate($request,[
            'title'=>'string|required',
            'summary'=>'string|nullable',
            'photo'=>'max:2048',
            'status'=>'required|in:active,inactive',
            //'is_parent'=>'sometimes|in:1',
            'parent_id'=>'required',
        ]);
        $data= $request->all();
        $data['is_parent']=$request->input('is_parent',0);

        if(isset($data['photo']))
        {
            $image_original = $request->file('photo');
            $image = time().'.'.$request->photo->getClientOriginalName();
            $data['photo'] = $image;
        }

        $status=$category->fill($data)->save();
        if($status){
            if(isset($data['photo']))
            {
                $image_original->move(public_path('img/category_images'), $image);
            }
            request()->session()->flash('success','Variety successfully updated');
        }
        else{
            request()->session()->flash('error','Error occurred, Please try again!');
        }
        return redirect()->route('variety.index');
    }

}
