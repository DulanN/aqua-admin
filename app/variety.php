<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class variety extends Model
{
    protected $fillable=['title','slug','summary','photo','status','parent_id'];

    public function parent_info(){
        return $this->hasOne('App\variety','id','status');
    }

    public static function getAllCategory(){
        return  variety::orderBy('id','DESC')->with('parent_info')->paginate(10);
    }
}
